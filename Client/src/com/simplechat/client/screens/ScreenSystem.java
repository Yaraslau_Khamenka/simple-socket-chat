package com.simplechat.client.screens;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ScreenSystem {
    public static final int SCREEN_WIDTH    = 1024;
    public static final int SCREEN_HEIGHT   = 600;

    private Map<ScreenType, Scene> screens;
    private Stage window;

    private static volatile ScreenSystem instance = null;

    private ScreenSystem() {
        screens = new HashMap<>();
    }

    public static ScreenSystem getInstance() {
        if (instance == null){
            synchronized (ScreenSystem.class){
                if (instance == null){
                    instance = new ScreenSystem();
                }
            }
        }

        return instance;
    }

    public void switchScreen(ScreenType screenType) throws IOException {
        if (window != null){
            if (screens.get(screenType) != null){
                window.setScene(screens.get(screenType));
            } else {
                window.setScene(createScene(screenType));

            }
            if (!window.isShowing()){
                window.show();
            }
        }
    }

    private Scene createScene(ScreenType screenType) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(String.format("/views/%s.fxml", screenType.getViewName())));
        Scene scene = new Scene(root, SCREEN_WIDTH, SCREEN_HEIGHT);
        screens.put(screenType, scene);
        return scene;
    }

    public void registerWindow(Stage window){
        this.window = window;
    }

    public Stage getWindow() {
        return window;
    }
}