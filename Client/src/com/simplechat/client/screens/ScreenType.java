package com.simplechat.client.screens;

public enum ScreenType {
    SETTINGS_SCREEN {
        @Override
        public String getViewName() {
            return "SettingsScreenView";
        }
    },
    MAIN_SCREEN {
        @Override
        public String getViewName() {
            return "MainScreenView";
        }
    };

    public abstract String getViewName();
}
