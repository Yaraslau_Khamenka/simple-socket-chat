package com.simplechat.client;

import com.simplechat.client.screens.ScreenSystem;
import com.simplechat.client.screens.ScreenType;
import com.simplechat.client.info.Options;
import javafx.application.Application;
import javafx.stage.Stage;

public class ClientApp extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage window) throws Exception {
        ScreenSystem.getInstance().registerWindow(window);
        ScreenSystem.getInstance().switchScreen(ScreenType.SETTINGS_SCREEN);
    }

    @Override
    public void stop() throws Exception {
        Options.getInstance().getClientThread().finish();
    }

}
