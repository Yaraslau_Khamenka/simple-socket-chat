package com.simplechat.client.info;

import com.simplechat.client.threading.ClientThread;

public class Options {
    private UserInfo userInfo = null;
    private ClientThread clientThread;
    private static volatile Options instance = null;

    private Options() {
    }

    public static Options getInstance() {
        if (instance == null) {
            synchronized (Options.class) {
                if (instance == null) {
                    instance = new Options();
                }
            }
        }
        return instance;
    }

    public ClientThread getClientThread() {
        return clientThread;
    }

    public void setClientThread(ClientThread clientThread) {
        this.clientThread = clientThread;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

}
