package com.simplechat.client.info;

public class UserInfo {
    private String userName;
    private String hostAddress;
    private int port;

    public UserInfo(String userName, String hostAddress, int port) {
        this.userName = userName;
        this.hostAddress = hostAddress;
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
