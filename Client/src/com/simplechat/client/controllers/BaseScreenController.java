package com.simplechat.client.controllers;

import com.simplechat.client.screens.ScreenSystem;
import com.simplechat.client.screens.ScreenType;
import javafx.fxml.Initializable;

import java.io.IOException;

public abstract class BaseScreenController implements Initializable {

    protected void navigate(ScreenType screenType) {
        try {
            ScreenSystem.getInstance().switchScreen(screenType);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
