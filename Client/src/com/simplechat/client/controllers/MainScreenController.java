package com.simplechat.client.controllers;

import com.simplechat.client.screens.ScreenSystem;
import com.simplechat.client.utils.myListCell;
import com.simplechat.client.info.Options;
import com.simplechat.commons.classes.UserOnline;
import com.simplechat.commons.interfaces.IMessageSender;
import com.simplechat.commons.interfaces.MessageDispatcher;
import com.simplechat.commons.messaging.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainScreenController<T extends Thread & IMessageSender> extends BaseScreenController implements MessageDispatcher<T> {
    private String recipientId;
    private ObservableList<UserOnline> users = FXCollections.observableArrayList();

    @FXML
    private ListView<UserOnline> usersList;
    @FXML
    private TextArea chatArea;
    @FXML
    private TextArea msgEdit;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ScreenSystem.getInstance().getWindow().setTitle(
                "simple chat for " +
                        Options.getInstance().getUserInfo().getUserName()
        );

        recipientId = null;

        usersList.setItems(users);
        usersList.setCellFactory(new Callback<ListView<UserOnline>, ListCell<UserOnline>>() {
            @Override
            public ListCell<UserOnline> call(ListView<UserOnline> listView) {
                return new myListCell();
            }
        });

        Options.getInstance().getClientThread().setMessageEventListener(this);
        Options.getInstance().getClientThread().start();

        try {
            Options.getInstance().getClientThread().sendMessage(new IntroduceMessage(
                    Options.getInstance().getUserInfo().getUserName()));
        } catch (IOException e) {
            System.out.println("IntroduceMessage was not sent.");
            e.printStackTrace();
        }
    }

    @FXML
    public void listViewReleased(MouseEvent event) {
        if (usersList.getSelectionModel().getSelectedItem() != null) {
            recipientId = usersList.getSelectionModel().getSelectedItem().getUserId();
        } else {
            recipientId = null;
        }
    }

    @FXML
    private void onSendBtnClicked(MouseEvent mouseEvent) {
        sendMessage();
    }


    public void msgEditKeyboardReleased(KeyEvent keyEvent) {
        if (keyEvent.isAltDown() && keyEvent.getCode() == KeyCode.ENTER) {
            sendMessage();
        }
    }

    private void sendMessage() {
        try {
            Options.getInstance().getClientThread()
                    .sendMessage(new ClientMessage(msgEdit.getText(), recipientId));
        } catch (IOException e) {
            System.out.println("ClientMessage was not sent.");
            e.printStackTrace();
        }
        msgEdit.clear();
    }

    @Override
    public void onMessageReceived(BaseMessage message) throws IOException {
        Platform.runLater(() -> {
            switch (message.getMessageType()) {
                case ONLINE_USERS_LIST_MESSAGE:
                    users.clear();
                    users.addAll(((OnlineUsersListMessage) message).getUsers());
                    break;
                default:
                    break;
            }
            chatArea.appendText(message.toString() + "\n");
        });
    }

    @Override
    public void killMessageSender(T messageSender) {
        Options.getInstance().getClientThread().finish();
    }

}