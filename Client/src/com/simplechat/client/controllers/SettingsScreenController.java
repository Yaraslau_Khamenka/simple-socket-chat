package com.simplechat.client.controllers;

import com.simplechat.client.screens.ScreenType;
import com.simplechat.client.threading.ClientThread;
import com.simplechat.client.info.Options;
import com.simplechat.client.info.UserInfo;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsScreenController extends BaseScreenController {

    @FXML
    private Label errMsgText;
    @FXML
    private TextField loginText;
    @FXML
    private TextField hostText;
    @FXML
    private TextField portText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errMsgText.setStyle("-fx-text-fill: red;");
    }

    @FXML
    private void onConnectBtnClicked(MouseEvent mouseEvent) {
        if (loginText.getText().equals("") ||
                hostText.getText().equals("") ||
                portText.getText().equals("")
        ) {
            errMsgText.setText("please fill in all fields");
            return;
        }

        try {
            Options.getInstance().setUserInfo(new UserInfo(loginText.getText(),
                hostText.getText(), Integer.parseInt(portText.getText())));
            Options.getInstance().setClientThread(new ClientThread(Options.getInstance().getUserInfo()));

            navigate(ScreenType.MAIN_SCREEN);
        } catch (IOException e) {
            e.printStackTrace();
            errMsgText.setText(e.getMessage());
        } catch (NumberFormatException e){
            e.printStackTrace();
            errMsgText.setText(e.getMessage()+ "enter the number.");
        }
    }
}

