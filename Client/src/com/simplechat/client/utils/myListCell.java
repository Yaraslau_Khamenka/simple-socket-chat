package com.simplechat.client.utils;

import com.simplechat.commons.classes.UserOnline;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class myListCell extends ListCell<UserOnline> {
    private HBox hBox;
    private VBox vBox;
    private Label userName;
    private Label textAnimal;
    private ImageView imgAnimal;

    public myListCell() {
        try {
            hBox = FXMLLoader.load(getClass().getResource("/views/listViewCell.fxml"));

            vBox = (VBox) hBox.getChildren().get(0);
            userName = (Label) vBox.getChildren().get(0);
            textAnimal = (Label) vBox.getChildren().get(1);
            imgAnimal = (ImageView) hBox.getChildren().get(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateItem(UserOnline user, boolean empty) {
        super.updateItem(user, empty);

        if (user != null && !empty) {
            setGraphic(hBox);
            userName.setText(user.toString());
            textAnimal.setText("anonymous " + user.getTotem().toString());
            try {
                imgAnimal.setImage(getAnimalImage(user.getTotem().toString()));
            } catch (IOException e) {
                System.out.println("Can not find a picture!");
                e.printStackTrace();
            }
        } else {
            setGraphic(null);
            return;
        }
    }

    private Image getAnimalImage(String animal) throws IOException {
        return new Image(String.format("/img/animals/%s.png", animal));
    }

}
