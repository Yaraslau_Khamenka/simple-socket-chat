<h2>Info</h2>
This project is part of my learn of java.<br>
I store it as an example of using some technologies and algorithms:<br>
1. sockets
2. JavaFX
3. MVC
4. singleton
5. callback

<h2>How to start this app in your IntelliJ Idea?</h2>
Project was built in learn-aim so it not easy to install.<br>

Follow these steps:
1. clone this project
2. add three modules in "project structure": Client, Server & Common types
3. add dependencies: Server -> Common types & Client -> Common types
4. create an output path if you need one
5. mark "resources" directory as resources root
6. create run configuration: add app Server & app Client
7. start server and input port number
8. start client and input id, port & your name
9. start another client.

Done!

<h2>Help</h2>
Perhaps the biggest mistake was not to leave comments and notes.<br>
If you have a questions, feel free to contact me _yaraslau.khamenka@gmail.com_<br>

For Russian-speaking friends, these hieroglyphs may be useful :)<br>
<a href="http://vfl.ru/fotos/76ce538f19919851.html">
<img src="//images.vfl.ru/ii/1514239120/76ce538f/19919851_m.jpg" alt="send message" title="send message" border="0"></a>