package com.simplechat.server;

import com.simplechat.server.threading.Server;

import java.io.IOException;
import java.util.Scanner;

public class ServerApp {

    public static void main(String[] args) {

        ServerApp serverApp = new ServerApp();
        System.out.println("Server is ready for start.");
        serverApp.runTheApp(serverApp.readUserInput());
    }

    private void runTheApp(String portStringValue) {
        try {
            int port = Integer.parseInt(portStringValue);
            if (port > 1024 && port < 65536) {
                startServer(port);
                return;
            } else {
                System.out.println("please enter valid value between 1025 and 65535");
                runTheApp(readUserInput());
            }
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            runTheApp(readUserInput());
        }
    }

    private void startServer(int port) {
        try {
            Server server = new Server(port);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
            runTheApp(readUserInput());
        }
    }

    private String readUserInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("enter port value, please: ");

        return scanner.next();
    }
}
