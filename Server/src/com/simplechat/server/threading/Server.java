package com.simplechat.server.threading;

import com.simplechat.commons.messaging.MessageDispatcherImpl;

import java.io.IOException;
import java.net.ServerSocket;

public class Server extends Thread {
    private boolean isRunning;
    private ServerSocket serverSocket;
    private MessageDispatcherImpl<ClientThread> messageDispatcher;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        messageDispatcher = new MessageDispatcherImpl<>();
    }

    @Override
    public void run() {
        System.out.println("Server started at " + serverSocket.getLocalPort());
        isRunning = true;
        while (isRunning) {
            try {
                if (serverSocket != null) {
                    messageDispatcher.registerClientThread(new ClientThread(serverSocket.accept()));
                    System.out.println("New connection received.");
                }
                Thread.sleep(10);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Server stopped.");
    }
}
