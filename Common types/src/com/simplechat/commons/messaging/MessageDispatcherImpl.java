package com.simplechat.commons.messaging;

import com.simplechat.commons.classes.UserOnline;
import com.simplechat.commons.interfaces.IMessageSender;
import com.simplechat.commons.interfaces.MessageDispatcher;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MessageDispatcherImpl<T extends Thread & IMessageSender> implements MessageDispatcher<T> {
    private Map<String, T> clientsMap;

    public MessageDispatcherImpl() {
        clientsMap = new ConcurrentHashMap<>();
    }

    @Override
    public void onMessageReceived(BaseMessage message, T client) throws IOException {
        switch (message.getMessageType()) {
            case CLIENT_MESSAGE:
                ((ClientMessage) message).setSenderName(client.getClientName());

                if (((ClientMessage) message).getRecipientId() == null) {
                    // to all
                    for (T t : clientsMap.values()) {
                        t.sendMessage(message);
                    }
                } else {
                    // to recipient...
                    if (!((ClientMessage) message).getRecipientId().equals(client.getClientId())) {
                        clientsMap.get(((ClientMessage) message).getRecipientId()).sendMessage(message);
                    }
                    //... and user
                    client.sendMessage(message);
                }
                break;
            case INTRODUCE_MESSAGE:
                client.setClientName(((IntroduceMessage) message).getSenderName());

                new Thread(() -> {
                    try {
                        updateOnlineUsersList(client, " connected to us!");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
                break;
        }
    }

    @Override
    public void killMessageSender(T messageSender) {
        clientsMap.remove(messageSender.getClientId());
        new Thread(() -> {
            try {
                updateOnlineUsersList(messageSender, " leave us...");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        messageSender.finish();
    }

    @Override
    public void registerClientThread(T clientThread) {
        clientThread.setMessageEventListener(this);
        clientsMap.put(clientThread.getClientId(), clientThread);
        clientThread.start();
    }

    private void updateOnlineUsersList(T client, String messageBody) throws IOException {
        List<UserOnline> users = new LinkedList<>();
        // Who is online?
        for (String s : clientsMap.keySet()) {
            UserOnline user = new UserOnline(
                    clientsMap.get(s).getClientName(),
                    clientsMap.get(s).getClientId(),
                    clientsMap.get(s).getClientTotem()
            );
            users.add(user);
        }
        OnlineUsersListMessage msg = new OnlineUsersListMessage(
                client.getClientName() + messageBody,
                users
        );
        // Send list of users online.
        for (T t : clientsMap.values()) {
            t.sendMessage(msg);
        }
    }

}