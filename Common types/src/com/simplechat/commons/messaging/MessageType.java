package com.simplechat.commons.messaging;

public enum MessageType {
    CLIENT_MESSAGE,
    INTRODUCE_MESSAGE,
    ONLINE_USERS_LIST_MESSAGE
}