package com.simplechat.commons.messaging;

import java.io.Serializable;

public abstract class BaseMessage implements Serializable {

    private static final long serialVersionUID = -4057293652512913693L;
    private MessageType messageType;
    private long creationTime;
    protected String messageBody;

    private BaseMessage(MessageType messageType) {
        this.messageType = messageType;
    }

    public BaseMessage(MessageType messageType, String messageBody) {
        this(messageType);
        this.creationTime = System.currentTimeMillis();
        this.messageBody = messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public MessageType getMessageType() {
        return messageType;
    }

}
