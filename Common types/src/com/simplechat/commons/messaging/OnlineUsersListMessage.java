package com.simplechat.commons.messaging;

import com.simplechat.commons.classes.UserOnline;

import java.util.List;

public class OnlineUsersListMessage extends BaseMessage {
    private static final long serialVersionUID = 3093634354808178324L;
    private List<UserOnline> users;

    private OnlineUsersListMessage(String messageBody) {
        super(MessageType.ONLINE_USERS_LIST_MESSAGE, messageBody);
    }

    public OnlineUsersListMessage(String messageBody, List<UserOnline> users) {
        this(messageBody);
        this.users = users;
    }

    public List<UserOnline> getUsers() {
        return users;
    }

    @Override
    public String toString() {
        return this.getMessageBody();
    }
}
