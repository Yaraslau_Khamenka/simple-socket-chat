package com.simplechat.commons.classes;

import java.io.Serializable;

public class UserOnline implements Serializable {
    private static final long serialVersionUID = -4808668674477069692L;
    private String userName;
    private String userId;
    private Animal totem;

    public UserOnline(String userName, String userId, Animal totem) {
        this.userName = userName;
        this.userId = userId;
        this.totem = totem;
    }

    public Animal getTotem() {
        return totem;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserOnline that = (UserOnline) o;

        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        return totem == that.totem;
    }

    @Override
    public int hashCode() {
        int result = userName != null ? userName.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (totem != null ? totem.hashCode() : 0);
        return result;
    }
}
