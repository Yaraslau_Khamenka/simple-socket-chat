package com.simplechat.commons.classes;

public enum Animal {
    AUROCH,
    AXOLOTL,
    CHAMELEON,
    CHEETAH,
    CORMORANT,
    DUCK,
    ELEPHANT,
    FOX,
    GRIZZLY,
    HYENA,
    IFRIT,
    MINK,
    MONKEY,
    RHINO,
    SKUNK,
    SQUIRREL,
    TURTLE,
    WALRUS,
    WOLF;

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }

    public Animal getRandomAnimal() {
        Animal[] animals = Animal.values();
        return animals[(int)(Math.random()*animals.length)];
    }
}
